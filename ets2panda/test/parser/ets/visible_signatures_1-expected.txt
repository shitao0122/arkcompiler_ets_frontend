{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 13
                },
                "end": {
                  "line": 17,
                  "column": 16
                }
              }
            },
            "kind": "method",
            "accessibility": "private",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 13
                    },
                    "end": {
                      "line": 17,
                      "column": 16
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "ETSUnionType",
                        "types": [
                          {
                            "type": "ETSTypeReference",
                            "part": {
                              "type": "ETSTypeReferencePart",
                              "name": {
                                "type": "Identifier",
                                "name": "Double",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 17,
                                    "column": 20
                                  },
                                  "end": {
                                    "line": 17,
                                    "column": 26
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 17,
                                  "column": 20
                                },
                                "end": {
                                  "line": 17,
                                  "column": 28
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 20
                              },
                              "end": {
                                "line": 17,
                                "column": 28
                              }
                            }
                          },
                          {
                            "type": "ETSUndefinedType",
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 29
                              },
                              "end": {
                                "line": 17,
                                "column": 38
                              }
                            }
                          }
                        ],
                        "loc": {
                          "start": {
                            "line": 17,
                            "column": 20
                          },
                          "end": {
                            "line": 17,
                            "column": 38
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 17,
                          "column": 17
                        },
                        "end": {
                          "line": 17,
                          "column": 38
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 17,
                        "column": 17
                      },
                      "end": {
                        "line": 17,
                        "column": 38
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 42
                    },
                    "end": {
                      "line": 17,
                      "column": 45
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "NumberLiteral",
                        "value": 0,
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 16
                          },
                          "end": {
                            "line": 18,
                            "column": 17
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 9
                        },
                        "end": {
                          "line": 18,
                          "column": 18
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 46
                    },
                    "end": {
                      "line": 19,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 16
                  },
                  "end": {
                    "line": 19,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 16
                },
                "end": {
                  "line": 19,
                  "column": 6
                }
              }
            },
            "overloads": [
              {
                "type": "MethodDefinition",
                "key": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 13
                    },
                    "end": {
                      "line": 20,
                      "column": 16
                    }
                  }
                },
                "kind": "method",
                "accessibility": "private",
                "static": false,
                "optional": false,
                "computed": false,
                "value": {
                  "type": "FunctionExpression",
                  "function": {
                    "type": "ScriptFunction",
                    "id": {
                      "type": "Identifier",
                      "name": "foo",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 13
                        },
                        "end": {
                          "line": 20,
                          "column": 16
                        }
                      }
                    },
                    "generator": false,
                    "async": false,
                    "expression": false,
                    "params": [
                      {
                        "type": "ETSParameterExpression",
                        "name": {
                          "type": "Identifier",
                          "name": "a",
                          "typeAnnotation": {
                            "type": "ETSPrimitiveType",
                            "loc": {
                              "start": {
                                "line": 20,
                                "column": 20
                              },
                              "end": {
                                "line": 20,
                                "column": 26
                              }
                            }
                          },
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 20,
                              "column": 17
                            },
                            "end": {
                              "line": 20,
                              "column": 26
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 20,
                            "column": 17
                          },
                          "end": {
                            "line": 20,
                            "column": 26
                          }
                        }
                      }
                    ],
                    "returnType": {
                      "type": "ETSPrimitiveType",
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 30
                        },
                        "end": {
                          "line": 20,
                          "column": 33
                        }
                      }
                    },
                    "body": {
                      "type": "BlockStatement",
                      "statements": [
                        {
                          "type": "ReturnStatement",
                          "argument": {
                            "type": "NumberLiteral",
                            "value": 1,
                            "loc": {
                              "start": {
                                "line": 21,
                                "column": 16
                              },
                              "end": {
                                "line": 21,
                                "column": 17
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 21,
                              "column": 9
                            },
                            "end": {
                              "line": 21,
                              "column": 18
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 34
                        },
                        "end": {
                          "line": 22,
                          "column": 6
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 20,
                        "column": 16
                      },
                      "end": {
                        "line": 22,
                        "column": 6
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 16
                    },
                    "end": {
                      "line": 22,
                      "column": 6
                    }
                  }
                },
                "overloads": [],
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 5
                  },
                  "end": {
                    "line": 22,
                    "column": 6
                  }
                }
              }
            ],
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 19,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 23,
                "column": 2
              },
              "end": {
                "line": 23,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 9
          },
          "end": {
            "line": 23,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 23,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 25,
                  "column": 10
                },
                "end": {
                  "line": 25,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 25,
                      "column": 10
                    },
                    "end": {
                      "line": 25,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 25,
                      "column": 18
                    },
                    "end": {
                      "line": 25,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "CallExpression",
                        "callee": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "ETSNewClassInstanceExpression",
                            "typeReference": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "A",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 26,
                                      "column": 9
                                    },
                                    "end": {
                                      "line": 26,
                                      "column": 10
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 26,
                                    "column": 9
                                  },
                                  "end": {
                                    "line": 26,
                                    "column": 11
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 26,
                                  "column": 9
                                },
                                "end": {
                                  "line": 26,
                                  "column": 11
                                }
                              }
                            },
                            "arguments": [],
                            "loc": {
                              "start": {
                                "line": 26,
                                "column": 5
                              },
                              "end": {
                                "line": 26,
                                "column": 13
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "foo",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 26,
                                "column": 13
                              },
                              "end": {
                                "line": 26,
                                "column": 16
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 26,
                              "column": 5
                            },
                            "end": {
                              "line": 26,
                              "column": 16
                            }
                          }
                        },
                        "arguments": [
                          {
                            "type": "NumberLiteral",
                            "value": 2,
                            "loc": {
                              "start": {
                                "line": 26,
                                "column": 17
                              },
                              "end": {
                                "line": 26,
                                "column": 20
                              }
                            }
                          }
                        ],
                        "optional": false,
                        "loc": {
                          "start": {
                            "line": 26,
                            "column": 5
                          },
                          "end": {
                            "line": 26,
                            "column": 21
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 26,
                          "column": 5
                        },
                        "end": {
                          "line": 26,
                          "column": 21
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 25,
                      "column": 23
                    },
                    "end": {
                      "line": 27,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 25,
                    "column": 14
                  },
                  "end": {
                    "line": 27,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 25,
                  "column": 14
                },
                "end": {
                  "line": 27,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 25,
                "column": 1
              },
              "end": {
                "line": 27,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 29,
      "column": 1
    }
  }
}
TypeError: Signature foo(a: double): int is not visible here. [visible_signatures_1.ets:26:5]
