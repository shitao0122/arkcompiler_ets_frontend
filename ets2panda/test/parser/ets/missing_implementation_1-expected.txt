{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "instance_field",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "instance_field",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "number",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 17,
                          "column": 21
                        },
                        "end": {
                          "line": 17,
                          "column": 27
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 17,
                        "column": 21
                      },
                      "end": {
                        "line": 18,
                        "column": 2
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 21
                    },
                    "end": {
                      "line": 18,
                      "column": 2
                    }
                  }
                },
                "declare": true,
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 5
                  },
                  "end": {
                    "line": 18,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 2
                }
              }
            },
            "overloads": [
              {
                "type": "MethodDefinition",
                "key": {
                  "type": "Identifier",
                  "name": "instance_field",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "kind": "method",
                "accessibility": "public",
                "static": false,
                "optional": false,
                "computed": false,
                "value": {
                  "type": "FunctionExpression",
                  "function": {
                    "type": "ScriptFunction",
                    "id": {
                      "type": "Identifier",
                      "name": "instance_field",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 1
                        },
                        "end": {
                          "line": 1,
                          "column": 1
                        }
                      }
                    },
                    "generator": false,
                    "async": false,
                    "expression": false,
                    "params": [
                      {
                        "type": "ETSParameterExpression",
                        "name": {
                          "type": "Identifier",
                          "name": "instance_field",
                          "typeAnnotation": {
                            "type": "ETSTypeReference",
                            "part": {
                              "type": "ETSTypeReferencePart",
                              "name": {
                                "type": "Identifier",
                                "name": "number",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 17,
                                    "column": 21
                                  },
                                  "end": {
                                    "line": 17,
                                    "column": 27
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 17,
                                  "column": 21
                                },
                                "end": {
                                  "line": 18,
                                  "column": 2
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 21
                              },
                              "end": {
                                "line": 18,
                                "column": 2
                              }
                            }
                          },
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 17,
                              "column": 5
                            },
                            "end": {
                              "line": 17,
                              "column": 19
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 17,
                            "column": 5
                          },
                          "end": {
                            "line": 17,
                            "column": 19
                          }
                        }
                      }
                    ],
                    "declare": true,
                    "loc": {
                      "start": {
                        "line": 17,
                        "column": 5
                      },
                      "end": {
                        "line": 18,
                        "column": 2
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 5
                    },
                    "end": {
                      "line": 18,
                      "column": 2
                    }
                  }
                },
                "overloads": [],
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 5
                  },
                  "end": {
                    "line": 18,
                    "column": 2
                  }
                }
              }
            ],
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "instance_field",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "instance_field",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "instance_field",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "number",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 21
                              },
                              "end": {
                                "line": 17,
                                "column": 27
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 17,
                              "column": 21
                            },
                            "end": {
                              "line": 18,
                              "column": 2
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 17,
                            "column": 21
                          },
                          "end": {
                            "line": 18,
                            "column": 2
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 17,
                          "column": 5
                        },
                        "end": {
                          "line": 17,
                          "column": 19
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 17,
                        "column": 5
                      },
                      "end": {
                        "line": 17,
                        "column": 19
                      }
                    }
                  }
                ],
                "declare": true,
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 5
                  },
                  "end": {
                    "line": 18,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 21
          },
          "end": {
            "line": 18,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "Interface",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 11
          },
          "end": {
            "line": 16,
            "column": 20
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 20,
          "column": 6
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "Derived",
          "decorators": [],
          "loc": {
            "start": {
              "line": 20,
              "column": 7
            },
            "end": {
              "line": 20,
              "column": 14
            }
          }
        },
        "superClass": null,
        "implements": [
          {
            "type": "TSClassImplements",
            "expression": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "Interface",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 26
                    },
                    "end": {
                      "line": 20,
                      "column": 35
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 26
                  },
                  "end": {
                    "line": 20,
                    "column": 37
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 20,
                  "column": 26
                },
                "end": {
                  "line": 20,
                  "column": 37
                }
              }
            },
            "loc": {
              "start": {
                "line": 20,
                "column": 26
              },
              "end": {
                "line": 20,
                "column": 37
              }
            }
          }
        ],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "instance_field_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 21,
                  "column": 11
                },
                "end": {
                  "line": 21,
                  "column": 26
                }
              }
            },
            "value": {
              "type": "UnaryExpression",
              "operator": "-",
              "prefix": true,
              "argument": {
                "type": "NumberLiteral",
                "value": 1,
                "loc": {
                  "start": {
                    "line": 21,
                    "column": 38
                  },
                  "end": {
                    "line": 21,
                    "column": 39
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 21,
                  "column": 37
                },
                "end": {
                  "line": 21,
                  "column": 39
                }
              }
            },
            "accessibility": "private",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "number",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 21,
                      "column": 28
                    },
                    "end": {
                      "line": 21,
                      "column": 34
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 21,
                    "column": 28
                  },
                  "end": {
                    "line": 21,
                    "column": 36
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 21,
                  "column": 28
                },
                "end": {
                  "line": 21,
                  "column": 36
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 21,
                "column": 11
              },
              "end": {
                "line": 21,
                "column": 39
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "instance_field",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "get",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "instance_field",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "number",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 24,
                          "column": 25
                        },
                        "end": {
                          "line": 24,
                          "column": 31
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 24,
                        "column": 25
                      },
                      "end": {
                        "line": 24,
                        "column": 33
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 24,
                      "column": 25
                    },
                    "end": {
                      "line": 24,
                      "column": 33
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "MemberExpression",
                        "object": {
                          "type": "ThisExpression",
                          "loc": {
                            "start": {
                              "line": 25,
                              "column": 12
                            },
                            "end": {
                              "line": 25,
                              "column": 16
                            }
                          }
                        },
                        "property": {
                          "type": "Identifier",
                          "name": "instance_field_",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 25,
                              "column": 17
                            },
                            "end": {
                              "line": 25,
                              "column": 32
                            }
                          }
                        },
                        "computed": false,
                        "optional": false,
                        "loc": {
                          "start": {
                            "line": 25,
                            "column": 12
                          },
                          "end": {
                            "line": 25,
                            "column": 32
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 25,
                          "column": 5
                        },
                        "end": {
                          "line": 25,
                          "column": 33
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 24,
                      "column": 32
                    },
                    "end": {
                      "line": 26,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 24,
                    "column": 21
                  },
                  "end": {
                    "line": 26,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 24,
                  "column": 21
                },
                "end": {
                  "line": 26,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 24,
                "column": 21
              },
              "end": {
                "line": 26,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 28,
                "column": 2
              },
              "end": {
                "line": 28,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 20,
            "column": 36
          },
          "end": {
            "line": 28,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 20,
          "column": 1
        },
        "end": {
          "line": 28,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 29,
      "column": 1
    }
  }
}
TypeError: Derived is not abstract and does not implement setter for instance_field property in Interface [missing_implementation_1.ets:20:36]
