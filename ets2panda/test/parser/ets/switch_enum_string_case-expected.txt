{
  "type": "Program",
  "statements": [
    {
      "type": "TSEnumDeclaration",
      "id": {
        "type": "Identifier",
        "name": "Enum",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 6
          },
          "end": {
            "line": 16,
            "column": 10
          }
        }
      },
      "members": [
        {
          "type": "TSEnumMember",
          "id": {
            "type": "Identifier",
            "name": "e1",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 12
              },
              "end": {
                "line": 16,
                "column": 14
              }
            }
          },
          "initializer": {
            "type": "StringLiteral",
            "value": "A",
            "loc": {
              "start": {
                "line": 16,
                "column": 17
              },
              "end": {
                "line": 16,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 12
            },
            "end": {
              "line": 16,
              "column": 20
            }
          }
        },
        {
          "type": "TSEnumMember",
          "id": {
            "type": "Identifier",
            "name": "e2",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 22
              },
              "end": {
                "line": 16,
                "column": 24
              }
            }
          },
          "initializer": {
            "type": "StringLiteral",
            "value": "B",
            "loc": {
              "start": {
                "line": 16,
                "column": 27
              },
              "end": {
                "line": 16,
                "column": 30
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 22
            },
            "end": {
              "line": 16,
              "column": 30
            }
          }
        },
        {
          "type": "TSEnumMember",
          "id": {
            "type": "Identifier",
            "name": "e3",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 32
              },
              "end": {
                "line": 16,
                "column": 34
              }
            }
          },
          "initializer": {
            "type": "StringLiteral",
            "value": "C",
            "loc": {
              "start": {
                "line": 16,
                "column": 37
              },
              "end": {
                "line": 16,
                "column": 40
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 32
            },
            "end": {
              "line": 16,
              "column": 40
            }
          }
        }
      ],
      "const": false,
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 41
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 10
                },
                "end": {
                  "line": 18,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 10
                    },
                    "end": {
                      "line": 18,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "v",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 7
                              },
                              "end": {
                                "line": 19,
                                "column": 8
                              }
                            }
                          },
                          "init": {
                            "type": "MemberExpression",
                            "object": {
                              "type": "Identifier",
                              "name": "Enum",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 19,
                                  "column": 11
                                },
                                "end": {
                                  "line": 19,
                                  "column": 15
                                }
                              }
                            },
                            "property": {
                              "type": "Identifier",
                              "name": "e3",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 19,
                                  "column": 16
                                },
                                "end": {
                                  "line": 19,
                                  "column": 18
                                }
                              }
                            },
                            "computed": false,
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 11
                              },
                              "end": {
                                "line": 19,
                                "column": 18
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 7
                            },
                            "end": {
                              "line": 19,
                              "column": 18
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 3
                        },
                        "end": {
                          "line": 19,
                          "column": 18
                        }
                      }
                    },
                    {
                      "type": "SwitchStatement",
                      "discriminant": {
                        "type": "Identifier",
                        "name": "v",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 20,
                            "column": 11
                          },
                          "end": {
                            "line": 20,
                            "column": 12
                          }
                        }
                      },
                      "cases": [
                        {
                          "type": "SwitchCase",
                          "test": {
                            "type": "MemberExpression",
                            "object": {
                              "type": "Identifier",
                              "name": "Enum",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 21,
                                  "column": 10
                                },
                                "end": {
                                  "line": 21,
                                  "column": 14
                                }
                              }
                            },
                            "property": {
                              "type": "Identifier",
                              "name": "e2",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 21,
                                  "column": 15
                                },
                                "end": {
                                  "line": 21,
                                  "column": 17
                                }
                              }
                            },
                            "computed": false,
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 21,
                                "column": 10
                              },
                              "end": {
                                "line": 21,
                                "column": 17
                              }
                            }
                          },
                          "consequent": [
                            {
                              "type": "AssertStatement",
                              "test": {
                                "type": "BooleanLiteral",
                                "value": false,
                                "loc": {
                                  "start": {
                                    "line": 21,
                                    "column": 26
                                  },
                                  "end": {
                                    "line": 21,
                                    "column": 31
                                  }
                                }
                              },
                              "second": null,
                              "loc": {
                                "start": {
                                  "line": 21,
                                  "column": 19
                                },
                                "end": {
                                  "line": 21,
                                  "column": 32
                                }
                              }
                            },
                            {
                              "type": "BreakStatement",
                              "label": null,
                              "loc": {
                                "start": {
                                  "line": 21,
                                  "column": 33
                                },
                                "end": {
                                  "line": 21,
                                  "column": 39
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 21,
                              "column": 5
                            },
                            "end": {
                              "line": 21,
                              "column": 39
                            }
                          }
                        },
                        {
                          "type": "SwitchCase",
                          "test": {
                            "type": "MemberExpression",
                            "object": {
                              "type": "Identifier",
                              "name": "Enum",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 22,
                                  "column": 10
                                },
                                "end": {
                                  "line": 22,
                                  "column": 14
                                }
                              }
                            },
                            "property": {
                              "type": "Identifier",
                              "name": "e1",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 22,
                                  "column": 15
                                },
                                "end": {
                                  "line": 22,
                                  "column": 17
                                }
                              }
                            },
                            "computed": false,
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 22,
                                "column": 10
                              },
                              "end": {
                                "line": 22,
                                "column": 17
                              }
                            }
                          },
                          "consequent": [
                            {
                              "type": "AssertStatement",
                              "test": {
                                "type": "BooleanLiteral",
                                "value": false,
                                "loc": {
                                  "start": {
                                    "line": 22,
                                    "column": 26
                                  },
                                  "end": {
                                    "line": 22,
                                    "column": 31
                                  }
                                }
                              },
                              "second": null,
                              "loc": {
                                "start": {
                                  "line": 22,
                                  "column": 19
                                },
                                "end": {
                                  "line": 22,
                                  "column": 32
                                }
                              }
                            },
                            {
                              "type": "BreakStatement",
                              "label": null,
                              "loc": {
                                "start": {
                                  "line": 22,
                                  "column": 33
                                },
                                "end": {
                                  "line": 22,
                                  "column": 39
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 22,
                              "column": 5
                            },
                            "end": {
                              "line": 22,
                              "column": 39
                            }
                          }
                        },
                        {
                          "type": "SwitchCase",
                          "test": {
                            "type": "MemberExpression",
                            "object": {
                              "type": "Identifier",
                              "name": "Enum",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 23,
                                  "column": 10
                                },
                                "end": {
                                  "line": 23,
                                  "column": 14
                                }
                              }
                            },
                            "property": {
                              "type": "Identifier",
                              "name": "e3",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 23,
                                  "column": 15
                                },
                                "end": {
                                  "line": 23,
                                  "column": 17
                                }
                              }
                            },
                            "computed": false,
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 23,
                                "column": 10
                              },
                              "end": {
                                "line": 23,
                                "column": 17
                              }
                            }
                          },
                          "consequent": [
                            {
                              "type": "BreakStatement",
                              "label": null,
                              "loc": {
                                "start": {
                                  "line": 23,
                                  "column": 20
                                },
                                "end": {
                                  "line": 23,
                                  "column": 26
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 23,
                              "column": 5
                            },
                            "end": {
                              "line": 23,
                              "column": 26
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 3
                        },
                        "end": {
                          "line": 24,
                          "column": 4
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 17
                    },
                    "end": {
                      "line": 25,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 14
                  },
                  "end": {
                    "line": 25,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 14
                },
                "end": {
                  "line": 25,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 1
              },
              "end": {
                "line": 25,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 26,
      "column": 1
    }
  }
}
